namespace Munsk
{
    /// <summary>
    /// The Direction `enum` allows the user to input directions for the player to move.
    /// </summary>
    internal enum Direction {
        Left,
        Right,
        Up,
        Down
    }
}