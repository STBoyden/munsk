using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Munsk {
    public class Main_Game : Game {
        public const float GRAVITY = 2f;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;

        Player player;
        Background background;

        float timer = 0;

        public Main_Game() {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.IsFullScreen = true;

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;
            TargetElapsedTime = TimeSpan.FromTicks(166666);
        }

        protected override void Initialize() {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            font = Content.Load<SpriteFont>("font");

            var playerTexture = Content.Load<Texture2D>("player_temp");

            player = new Player(playerTexture, new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2), GraphicsDevice.Viewport);

            background = new Background("floor_sprite_cross", GraphicsDevice.Viewport, Content);
            Background.Load();
        }

        protected override void Update(GameTime gameTime) {
            timer += (float) gameTime.ElapsedGameTime.TotalSeconds;
            var updateTime = 1f / 60;

            while (timer >= updateTime) {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                    Keyboard.GetState().IsKeyDown(Keys.Escape) ||
                    Keyboard.GetState().IsKeyDown(Keys.Q))
                    Exit();

                // TODO: Add your update logic here

                if (Keyboard.GetState().IsKeyDown(Keys.R))
                    player.Position = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);

                if (Keyboard.GetState().IsKeyDown(Keys.W))
                    player.Move(Direction.Up, player.Speed);

                if (Keyboard.GetState().IsKeyDown(Keys.S))
                    player.Move(Direction.Down, player.Speed);

                if (Keyboard.GetState().IsKeyDown(Keys.A))
                    player.Move(Direction.Left, player.Speed);

                if (Keyboard.GetState().IsKeyDown(Keys.D))
                    player.Move(Direction.Right, player.Speed);

                timer -= updateTime;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Transparent);

            var deltaTime = (float) gameTime.ElapsedGameTime.TotalSeconds;
            var framesPerSecond = 1.0f / deltaTime;

            // TODO: Add your drawing code here

            background.Draw(spriteBatch);
            player.Draw(spriteBatch);

            spriteBatch.Begin();
            spriteBatch.DrawString(font, "Width: " + GraphicsDevice.Viewport.Width + "  Height: " + GraphicsDevice.Viewport.Height, new Vector2(10, 10), Color.Black);
            spriteBatch.DrawString(font, "FPS: " + (int) framesPerSecond, new Vector2(10, GraphicsDevice.Viewport.Height - font.Texture.Height), Color.GreenYellow);
            spriteBatch.DrawString(font, "X: " + player.getXPos() + "  Y: " + player.getYPos(), new Vector2(10, 30), Color.Black);
            spriteBatch.DrawString(font, "Speed: " + player.Speed, new Vector2(10, 50), Color.Black);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}