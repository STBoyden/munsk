using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Munsk {
    internal class Background {
        public string TextureName { get; set; }
        public static List<Texture2D> Textures { get; set; } = new List<Texture2D>();
        public Viewport Window { get; set; }

        private static ContentManager Content { get; set; }

        /// <summary>
        /// This is the constructor for the Background class.
        /// </summary>
        /// <param name="textureName">The name of the texture to be used for the background.</param>
        /// <param name="window">The window in which the background is being drawn.</param>
        /// <param name="content">The content manager to be used.</param>
        public Background(string textureName, Viewport window, ContentManager content) {
            TextureName = textureName;
            Window = window;
            Content = content;
        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Begin();
            for (var j = 0; j < Window.Height;) {

                Floor floorSprite = new Floor(Textures.Find(texture => {
                    return texture.Name == "" ? true : false;
                }), new Vector2(0, 0));

                for (var i = 0; i < Window.Width;) {

                    floorSprite = new Floor(Textures.Find(texture => {
                        return texture.Name == TextureName ? true : false;
                    }), new Vector2(i, j));

                    floorSprite.Draw(spriteBatch);
                    i += floorSprite.Texture.Width;
                }
                j += floorSprite.Texture.Height;
            }
            spriteBatch.End();
        }

        public static void Load() {
            DirectoryInfo dir = new DirectoryInfo("Content");
            FileInfo[] files = dir.GetFiles();

            foreach (var file in files) {
                if (Regex.Match(file.Name, "floor_sprite_").Success)
                    Textures.Add(Content.Load<Texture2D>(file.Name.Replace(".png", "")));
            }
        }
    }
}