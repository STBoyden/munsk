using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Munsk {
    /// <summary>
    /// The Player class (used for the player(s) of the game).
    /// </summary>
    internal class Player {
        public float Health { get; set; } = 100f;
        public float Speed { get; set; } = 4f;
        public Texture2D Sprite { get; set; }
        public Vector2 Position { get; set; } = new Vector2(0, 0);
        public Viewport Window { get; set; }

        /// <summary>
        /// The constructor for the Player class.
        /// </summary>
        /// <param name="sprite">The sprite for the Player class to use.</param>
        /// <param name="pos">The Player's position in the worldspace.</param>
        /// <param name="window">The window in which the Player is being drawn in.</param>
        public Player(Texture2D sprite, Vector2 pos, Viewport window) {
            Sprite = sprite;
            Position = pos;
            Window = window;
        }

        /// <summary>
        /// Gets the X position of the player.
        /// </summary>
        public int getXPos() { return (int) Position.X; }
        /// <summary>
        /// Gets the Y position of the player.
        /// </summary>
        public int getYPos() { return (int) Position.Y; }

        /// <summary>
        /// Moves the player in a given direction by a given amount.
        /// </summary>
        /// <param name="direction">The direction in which the player is to move.</param>
        /// <param name="amount">The amount of which the player is to move by.</param>
        public void Move(Direction direction, float amount) {

            if (direction == Direction.Left) {
                if (Position.X != 0)
                    Position = new Vector2(Position.X - amount, Position.Y);
            } else if (direction == Direction.Right) {
                if (Position.X != Window.Width - Sprite.Width)
                    Position = new Vector2(Position.X + amount, Position.Y);
            }

            if (direction == Direction.Up) {
                if (Position.Y != 0)
                    Position = new Vector2(Position.X, Position.Y - amount);
            } else if (direction == Direction.Down) {
                if (Position.Y != Window.Height - Sprite.Height)
                    Position = new Vector2(Position.X, Position.Y + amount);
            }
        }

        /// <summary>
        /// The draw function of the Player class.
        /// </summary>
        /// <param name="sb">The spritebatch of the current game.</param>
        public void Draw(SpriteBatch sb) {
            sb.Begin();
            sb.Draw(texture: Sprite, position: Position);
            sb.End();
        }
    }
}