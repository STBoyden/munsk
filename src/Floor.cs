using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Munsk {
    internal class Floor {
        public Texture2D Texture { get; protected set; }
        public Vector2 Position { get; set; }

        /// <summary>
        /// This is the constructor for the Floor class.
        /// </summary>
        /// <param name="texture">The texture to be used for the floor sprite.</param>
        /// <param name="position">The position in the window in which to draw the sprite.</param>
        public Floor(Texture2D texture, Vector2 position) {
            Texture = texture;
            Position = position;
        }

        /// <summary>
        /// Draws the Floor class onto the screen.
        /// </summary>
        /// <param name="sb">The spritebatch of the current game.</param>        
        public void Draw(SpriteBatch sb) {
            sb.Draw(texture: Texture, position: Position);
        }
    }
}