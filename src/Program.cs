using System;

namespace Munsk {
    internal static class Program {
        [STAThread]
        static void Main() {
            using(var game = new Main_Game())
            game.Run();
        }
    }
}